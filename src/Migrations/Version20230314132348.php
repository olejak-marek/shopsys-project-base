<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Shopsys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20230314132348 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $czkCurrencyId = 1;
        $this->sql(
            'UPDATE setting_values
            SET value = ' . $czkCurrencyId . '
            WHERE name =  \'defaultCurrencyId\''
        );

        $this->sql(
            'UPDATE setting_values
            SET value = ' . $czkCurrencyId . '
            WHERE name = \'defaultDomainCurrencyId\''
        );
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
