<?php

declare(strict_types=1);

namespace App\Component\CnbExchangeRate\Exception;

use Throwable;

interface CnbExceptionInterface extends Throwable
{
}
